Source: phyloviz-core
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper (>= 10),
               javahelper,
               default-jdk,
               ant,
               libnb-platform13-java
Standards-Version: 3.9.8
Vcs-Browser: https://salsa.debian.org/med-team/phyloviz-core
Vcs-Git: https://salsa.debian.org/med-team/phyloviz-core.git
Homepage: http://www.phyloviz.net

Package: phyloviz-core
Architecture: all
Depends: ${misc:Depends},
         ${java:Depends}
Description: phylogenetic inference and data visualization for sequence based typing methods
 Phyloviz allows the analysis of sequence-based typing methods that
 generate allelic profiles and their associated epidemiological data.
 .
 For representing the possible evolutionary relationships between
 strains identified by allelic profiles it uses the goeBURST algorithm, a
 refinement of eBURST algorithm proposed by Feil et al., and its
 expansion to generate a complete minimum spanning tree (MST).
 .
 Phyloviz is being developed in a modular way to allow its expansion
 with novel data analysis algorithms and new visualization modules.
 .
 Capabilities
  * Modularity allows the creation of plugins to analyse different types of data
  * Allows the visualization of data overlaid onto goeBURST and MST results
  * Confidence assessment of each link in the graph
  * Query the data and see the query results directly onto the graphs
  * Search your data set using regular expressions to select what to display
  * Export the results as images in various formats: eps, png, gif, pdf, etc
